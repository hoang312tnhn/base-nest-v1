import Ajv from 'ajv';
import { validateConfigSchema } from './config.schema';
import { Exception } from '$common/filters/exceptions/exception';
import { ErrorCode } from '$common/constant/error';

export async function validateENV(config: Record<string, unknown>) {
  const AjvInstance = new Ajv();
  const validate = AjvInstance.validate(validateConfigSchema, config);
  if (!validate) {
    console.log(AjvInstance.errors);
    throw new Exception(ErrorCode.Invalid_Variable_Env);
  }
  return config;
}
