export default () => ({
  env: process.env.ENV,
  port: parseInt(process.env.PORT, 10) || 3000,
  database: {
    host: process.env.MYSQL_HOST,
    port: parseInt(process.env.MYSQL_PORT),
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DB,
  },
  auth: {
    private_key_token: process.env.PRIVATEKEY_TOKEN,
    token_expire_in: process.env.TOKEN_EXPIRE_IN,
    private_key_refresh_token: process.env.PRIVATEKEY_REFRESH_TOKEN,
    refresh_token_expire_in: process.env.REFRESH_TOKEN_EXPIRE_IN,
    salt_round: process.env.SALT_ROUND || 10,
  },
  auth_google: {
    client_id: process.env.CLIENT_ID_GOOGLE,
    client_secret: process.env.CLIENT_SECRET_GOOGLE,
    callback_url: process.env.CALLBACK_URL_GOOGLE,
  },
  redis: {},
  send_grid: {
    email: process.env.SEND_GRID_EMAIL,
    api_key: process.env.SEND_GRID_API_KEY,
  },
});
