export const validateConfigSchema: AjvSchema = {
  type: 'object',
  //additionalProperties: false,
  properties: {
    ENV: { type: 'string' },
    PORT: { type: 'string', pattern: '^[0-9]+$' },
    //DATABASE
    MYSQL_HOST: { type: 'string' },
    MYSQL_PORT: { type: 'string', pattern: '^[0-9]+$' },
    MYSQL_USER: { type: 'string' },
    MYSQL_PASSWORD: { type: 'string' },
    MYSQL_DB: { type: 'string' },
    //SEND_GRID
    SEND_GRID_EMAIL: { type: 'string' },
    SEND_GRID_API_KEY: {
      type: 'string',
    },
  },
};
