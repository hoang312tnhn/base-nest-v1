export interface AuthInterface {
  private_key_token: string;
  token_expire_in: number;
  private_key_refresh_token: string;
  refresh_token_expire_in: number;
  salt_round: number;
}
