export enum ErrorCode {
  UNKNOWN_ERROR = 0,
  NOT_ALLOW = 1,
  Invalid_Input = 2,
  Invalid_Variable_Env = 3,
  //---------------------User---------------------//
  User_Not_Found = 4,
  User_Inactive = 5,
  Password_Invalid = 6,
  Email_Existed = 7,
  Email_Invalid = 8,
  Refresh_Token_Expired = 9,
  Token_Expired = 10,
}

export const ErrorMessage = {
  '0': 'Lỗi không xác định',
  '1': 'Không được phép',
  '2': 'Lỗi dữ liệu đầu vào',
  '4': 'Không tìm thấy người dùng',
  '5': 'Người dùng bị khóa',
  '6': 'Mật khẩu không chính xác',
  '7': 'Email đã tồn tại',
  '8': 'Email không chính xác',
  '9': 'Hết phiên đăng nhập',
  '10': 'Mã token hết hạn',
};
