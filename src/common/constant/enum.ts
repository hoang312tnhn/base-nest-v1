export enum UserType {
  Client = 1,
  Staff = 2,
}

export enum CommonStatus {
  Active = 1,
  Inactive = 0,
  Delete = 3,
}

export enum Environment {
  Development = 'development',
  Production = 'production',
  Staging = 'staging',
}
