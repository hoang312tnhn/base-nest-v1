import { Controller } from '@nestjs/common/decorators';

const ClientController =
  (route: string): ClassDecorator =>
  (target: any) => {
    Controller(`client/${route}`)(target);
  };

export default ClientController;
