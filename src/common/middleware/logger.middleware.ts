import { NestMiddleware } from '@nestjs/common';
import { Injectable, Logger } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';

@Injectable()
export default class LoggerMiddleware implements NestMiddleware {
  private logger = new Logger('REQUEST');
  use(req: Request, res: Response, next: NextFunction) {
    this.logger.log(`Method:${req.method} URL: ${req.originalUrl}`);
    next();
  }
}
