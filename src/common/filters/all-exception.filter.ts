import { ErrorCode, ErrorMessage } from '$common/constant/error';
import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
  HttpException,
  Logger,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';
import { Response } from 'express';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  private logger = new Logger('ERROR');
  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}
  catch(exception: any, host: ArgumentsHost): void {
    const { httpAdapter } = this.httpAdapterHost;
    const ctx = host.switchToHttp();
    const errorCode = exception.errorCode;
    const errorMessage = exception.errorMessage;
    const httpStatus =
      exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.BAD_REQUEST;

    const responseBody = this.formatException(
      httpStatus,
      errorCode,
      errorMessage,
    );
    console.log(exception);
    this.logger.error(exception);
    httpAdapter.reply(ctx.getResponse(), responseBody, httpStatus);
  }
  formatException(
    statusCode: HttpStatus,
    errorCode?: ErrorCode,
    errorMessage?: string,
    errorDetail?: object,
  ) {
    errorCode = errorCode ? errorCode : ErrorCode.UNKNOWN_ERROR;
    errorMessage = errorMessage ? errorMessage : ErrorMessage[errorCode];
    errorDetail = errorDetail ? errorDetail : {};
    return {
      errorCode,
      errorMessage,
      errorDetail,
      statusCode,
    };
  }
}
