import { ErrorCode, ErrorMessage } from '$common/constant/error';
import { HttpStatus } from '@nestjs/common';
import { Exception } from './exception';

export class Unauthorized extends Exception {
  constructor() {
    super(
      ErrorCode.Token_Expired,
      ErrorMessage[ErrorCode.Token_Expired],
      HttpStatus.UNAUTHORIZED,
    );
  }
}
