import { ErrorMessage, ErrorCode } from '$common/constant/error';
import { HttpStatus, HttpException } from '@nestjs/common';
import { Exception } from './exception';
export class ForbiddenException extends Exception {
  constructor(errorCode?: number) {
    if (errorCode) super(errorCode);
    else super(ErrorCode.NOT_ALLOW);
    errorCode = errorCode ? errorCode : 0;
  }
}
