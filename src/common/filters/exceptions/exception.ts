import { ErrorMessage, ErrorCode } from '$common/constant/error';
import { HttpStatus } from '@nestjs/common';
import { HttpException } from '@nestjs/common';

export class Exception extends HttpException {
  errorCode: number;
  errorMessage: object | string;
  constructor(code?: number, message?: string | object, status?: HttpStatus) {
    const errorCode = code ? code : ErrorCode.UNKNOWN_ERROR;
    const statusRequest = status ? status : HttpStatus.BAD_REQUEST;
    const errorMessage = message ? message : ErrorMessage[errorCode];
    super(errorMessage, statusRequest);
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
  }
}
