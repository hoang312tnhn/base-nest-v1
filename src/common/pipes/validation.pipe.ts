import { ErrorCode } from '$common/constant/error';
import { Exception } from '$common/filters/exceptions/exception';
import { ArgumentMetadata } from '@nestjs/common';
import { PipeTransform } from '@nestjs/common';
import { Injectable } from '@nestjs/common';
import Ajv from 'ajv';
@Injectable()
export class ValidationPipe implements PipeTransform {
  private ajv: Ajv;
  constructor(private ajvObject: AjvSchema) {
    this.ajv = new Ajv();
  }
  transform(value, metadata: ArgumentMetadata) {
    const validate = this.ajv.compile(this.ajvObject);
    const valid = validate(value);

    if (!valid) {
      throw new Exception(ErrorCode.Invalid_Input, validate.errors);
    }
    return value;
  }
}
