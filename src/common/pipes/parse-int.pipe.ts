import { ErrorCode } from '$common/constant/error';
import { Exception } from '$common/filters/exceptions/exception';
import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class ParseIntPipe implements PipeTransform<string, number> {
  transform(value: string, metadata: ArgumentMetadata): number {
    const val = parseInt(value, 10);
    console.log(val);
    if (isNaN(val)) {
      throw new Exception(ErrorCode.Invalid_Input);
    }
    return val;
  }
}
