import { ErrorCode } from '$common/constant/error';
import { AuthInterface } from '$config/interface/auth';
import { Injectable } from '@nestjs/common';
import { ForbiddenException } from '@nestjs/common/exceptions';
import { ConfigService } from '@nestjs/config';
import { hash } from 'bcrypt';
@Injectable()
export class testConfigService {
  constructor(private configService: ConfigService) {}
  getHello() {
    throw new ForbiddenException(ErrorCode.NOT_ALLOW);
    return 'helloo';
  }

  async getError() {
    return await hash('asdfdsf', 'dc');
    // throw new ForbiddenException(ErrorCode.NOT_ALLOW);
  }

  getConfig() {
    throw new ForbiddenException(ErrorCode.NOT_ALLOW);
    const authEnv = this.configService.get<AuthInterface>('auth');
    return authEnv;
  }
}
