import { Module } from '@nestjs/common';
import { TestController } from './config.contreller';
import { testConfigService } from './config.service';

@Module({
  imports: [],
  controllers: [TestController],
  providers: [testConfigService],
})
export class TestConfig {}
