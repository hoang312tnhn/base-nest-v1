import { ParseIntPipe } from '$common/pipes/parse-int.pipe';
import { ValidationPipe } from '$common/pipes/validation.pipe';
import { TestSchemaDTO } from '$database/dto/testSchema.dto';
import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { testSchema } from './config.schema';
import { testConfigService } from './config.service';

@Controller()
export class TestController {
  constructor(private readonly testConfigService: testConfigService) {}

  @Get()
  getHello() {
    return this.testConfigService.getConfig();
  }

  @Post()
  testSchema(@Body(new ValidationPipe(testSchema)) data: TestSchemaDTO) {
    console.log(data);
    return this.testConfigService.getHello();
  }

  @Get('/error')
  getError() {
    return this.testConfigService.getError();
  }

  @Get('/:id')
  getDetail(@Param('id', new ParseIntPipe()) id: number) {
    return id;
  }
}
