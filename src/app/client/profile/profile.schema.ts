export const updateProfileSchema: AjvSchema = {
  type: 'object',
  properties: {
    firstName: {
      type: 'string',
    },
    lastName: {
      type: 'string',
    },
  },
};

export const changePasswordSchema: AjvSchema = {
  type: 'object',
  required: ['newPassword', 'oldPassword'],
  additionalProperties: false,
  properties: {
    oldPassword: {
      type: 'string',
    },
    newPassword: {
      type: 'string',
      minLength: 8,
    },
  },
};
