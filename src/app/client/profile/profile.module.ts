import { Photo } from '$database/entity/Photo';
import { User } from '$database/entity/User';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientProfileController } from './profile.controller';
import { ClientProfileService } from './profile.service';

@Module({
  imports: [TypeOrmModule.forFeature([User, Photo])],
  controllers: [ClientProfileController],
  providers: [ClientProfileService],
})
export class ClientProfileModule {}
