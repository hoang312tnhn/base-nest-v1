import {
  ChangePasswordDto,
  UserUpdateProfileDto,
} from '$database/dto/User.dto';
import { Photo } from '$database/entity/Photo';
import { User } from '$database/entity/User';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { compare, hash } from 'bcrypt';
import { Exception } from '$common/filters/exceptions/exception';
import { ErrorCode } from '$common/constant/error';
import { ConfigService } from '@nestjs/config';
@Injectable()
export class ClientProfileService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,

    @InjectRepository(Photo)
    private photoRepository: Repository<Photo>,

    private configService: ConfigService,
  ) {}

  async getProfile(id: string) {
    return await this.userRepository.findOne({ where: { id } });
  }

  async updateProfile(id: string, data: UserUpdateProfileDto) {
    await this.userRepository.update({ id }, data);
    return true;
  }

  async changePassword(id: string, data: ChangePasswordDto) {
    const { newPassword, oldPassword } = data;
    const salt_round = Number(this.configService.get('auth.salt_round'));
    const user = await this.userRepository.findOne({
      where: { id },
      select: ['password'],
    });
    const valid = await compare(oldPassword, user.password);
    if (!valid) throw new Exception(ErrorCode.Password_Invalid);
    const passwordHash = await hash(newPassword, salt_round);
    await this.userRepository.update({ id }, { password: passwordHash });
    return true;
  }
}
