import { ValidationPipe } from '$common/pipes/validation.pipe';
import {
  ChangePasswordDto,
  UserUpdateProfileDto,
} from '$database/dto/User.dto';
import { Controller, Get, Req, Put, Res, Body } from '@nestjs/common';
import { changePasswordSchema, updateProfileSchema } from './profile.schema';
import { ClientProfileService } from './profile.service';

@Controller('client/profile')
export class ClientProfileController {
  constructor(private clientProfileService: ClientProfileService) {}
  @Get('')
  getProfile(@Req() req) {
    return this.clientProfileService.getProfile(req.user.userId);
  }

  @Put('')
  updateProfile(
    @Body(new ValidationPipe(updateProfileSchema)) body: UserUpdateProfileDto,
    @Req() req,
  ) {
    return this.clientProfileService.updateProfile(req.user.userId, body);
  }

  @Put('/change-password')
  changePassword(
    @Body(new ValidationPipe(changePasswordSchema)) body: ChangePasswordDto,
    @Req() req,
  ) {
    return this.clientProfileService.changePassword(req.user.userId, body);
  }
}
