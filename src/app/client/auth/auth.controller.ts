import ClientController from '$common/decorator/client.decorator';
import { Public } from '$common/guards/public.decorator';
import { ValidationPipe } from '$common/pipes/validation.pipe';
import { UserLoginDto, UserRegisterDto } from '$database/dto/User.dto';
import {
  Body,
  Controller,
  Get,
  Post,
  Request,
  Response,
  UseGuards,
  Redirect,
} from '@nestjs/common/decorators';
import { AuthGuard } from '@nestjs/passport';
import { AuthLoginSchema, AuthRegisterSchema } from './auth.schema';
import { ClientAuthService } from './auth.service';

@Public()
@ClientController('auth')
export class ClientAuthController {
  constructor(private clientAuthService: ClientAuthService) {}
  @Post('/login')
  async login(@Body(new ValidationPipe(AuthLoginSchema)) body: UserLoginDto) {
    return this.clientAuthService.login(body);
  }

  @Get('/login/google')
  @UseGuards(AuthGuard('google'))
  async loginGoogle() {}

  @Get('/login/google/callback')
  @UseGuards(AuthGuard('google'))
  @Redirect()
  async loginGoogleCallback(@Request() req, @Response() res) {
    const { accessToken, refreshToken } =
      await this.clientAuthService.loginGoogle(req.user);
    res.redirect(
      `/login/?accessToken=${accessToken}&refreshToken=${refreshToken}`,
    );
  }

  @Get('/login')
  async loginTest(@Request() req) {
    console.log(req);
    return true;
  }

  @Post('/register')
  async register(
    @Body(new ValidationPipe(AuthRegisterSchema)) body: UserRegisterDto,
  ) {
    return this.clientAuthService.register(body);
  }

  @Post('/refresh-token')
  async refreshToken(
    @Body(
      new ValidationPipe({
        type: 'object',
        required: ['refreshToken'],
        properties: { refreshToken: { type: 'string' } },
      }),
    )
    body,
  ) {
    return this.clientAuthService.refreshToken(body.refreshToken);
  }
}
