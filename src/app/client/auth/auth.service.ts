import { CommonStatus } from '$common/constant/enum';
import { ErrorCode } from '$common/constant/error';
import { Exception } from '$common/filters/exceptions/exception';
import { UserLoginDto, UserRegisterDto } from '$database/dto/User.dto';
import { User } from '$database/entity/User';
import { generateRandomCharacter } from '$helper/ultil';
import { AuthService } from '$share/auth/auth.service';
import { SendGridService } from '$share/send-mail/sendGrid.service';
import { templateRegister } from '$template/mail';
import { Injectable } from '@nestjs/common/decorators';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { compare, hash } from 'bcrypt';
import { Not, Repository } from 'typeorm';
@Injectable()
export class ClientAuthService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private authService: AuthService,
    private config: ConfigService,
    private sendGrid: SendGridService,
  ) {}

  async login(data: UserLoginDto) {
    const { email, password } = data;
    const user = await this.userRepository.findOne({
      where: { email, status: Not(CommonStatus.Delete) },
      select: ['password', 'refreshToken', 'status', 'role', 'id'],
    });
    if (!user) throw new Exception(ErrorCode.User_Not_Found);
    if (user.status == CommonStatus.Inactive)
      throw new Exception(ErrorCode.User_Inactive);
    const verifyPassword = await compare(password, user.password);
    if (!verifyPassword) throw new Exception(ErrorCode.Password_Invalid);
    return await this.generateToken(user);
  }

  async register(data: UserRegisterDto) {
    const { email } = data;
    const salt_round = this.config.get<number>('auth.salt_round');
    const user = await this.userRepository.findOne({
      where: { email, status: Not(CommonStatus.Delete) },
      select: ['id'],
    });
    if (user) throw new Exception(ErrorCode.Email_Existed);
    const password = generateRandomCharacter(8);
    const passwordHash = await hash(password, Number(salt_round));
    await this.userRepository.save({ ...data, password: passwordHash });
    await this.sendGrid.send(
      email,
      'Register success',
      templateRegister(password),
    );
    return true;
  }

  async loginGoogle(data) {
    const { email, firstName, lastName } = data;
    const user = await this.userRepository.findOne({
      where: { email, status: Not(CommonStatus.Delete) },
      select: ['password', 'refreshToken', 'status', 'role', 'id'],
    });
    if (!user) {
      await this.userRepository.save({ email, firstName, lastName });
    }
    if (user.status == CommonStatus.Inactive)
      throw new Exception(ErrorCode.User_Inactive);
    return await this.generateToken(user);
  }

  async refreshToken(refreshToken: string) {
    const valid = await this.authService.verifyRefreshToken(refreshToken);
    if (!valid) throw new Exception(ErrorCode.Refresh_Token_Expired);
    const { userId, userType } = valid;
    const accessToken = await this.authService.createAccessToken({
      userId,
      userType,
    });
    return { accessToken };
  }
  async generateToken(payload: User) {
    const { refreshToken, role, id } = payload;
    let oldRefreshToken = refreshToken ? refreshToken : 'sad';

    let valid = false;
    if (refreshToken)
      valid = await this.authService.verifyRefreshToken(oldRefreshToken);
    if (!valid) {
      oldRefreshToken = await this.authService.createRefreshToken({
        userId: id,
        userType: role,
      });
    }
    this.userRepository.update({ id }, { refreshToken: oldRefreshToken });
    const accessToken = await this.authService.createAccessToken({
      userId: id,
      userType: role,
    });

    return { accessToken, refreshToken: oldRefreshToken };
  }
}
