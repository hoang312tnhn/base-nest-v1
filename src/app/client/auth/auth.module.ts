import { User } from '$database/entity/User';
import { AuthModule } from '$share/auth/auth.module';
import { Module } from '@nestjs/common/decorators';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClientAuthController } from './auth.controller';
import { ClientAuthService } from './auth.service';

@Module({
  imports: [TypeOrmModule.forFeature([User]), AuthModule],
  controllers: [ClientAuthController],
  providers: [ClientAuthService],
})
export class ClientAuthModule {}
