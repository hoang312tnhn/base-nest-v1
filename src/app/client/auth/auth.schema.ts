export const AuthLoginSchema: AjvSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    email: { type: 'string' },
    password: { type: 'string' },
  },
};
export const AuthRegisterSchema: AjvSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    email: { type: 'string' },
    firstName: { type: 'string' },
    lastName: { type: 'string' },
  },
};
