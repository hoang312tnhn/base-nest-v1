import { Module } from '@nestjs/common';
import { ClientAuthModule } from './auth/auth.module';
import { ClientAuthService } from './auth/auth.service';
import { ClientProfileModule } from './profile/profile.module';
import { TestConfig } from './test/config.module';

@Module({
  imports: [TestConfig, ClientProfileModule, ClientAuthModule],
})
export class CLientModule {}
