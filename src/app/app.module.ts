import { AllExceptionsFilter } from '$common/filters/all-exception.filter';
import { JwtAuthGuard } from '$common/guards/jwtAuthGuard.guard';
import { LoggingInterceptor } from '$common/Interceptors/loggingInterceptor';
import { TransformDataInterceptor } from '$common/Interceptors/transformInterceptor';
import LoggerMiddleware from '$common/middleware/logger.middleware';
import config from '$config/config';
import { DatabaseConfigInterface } from '$config/interface/database';
import { ListEntity } from '$database/entity';
import { ShareModule } from '$share/share.module';
import { MiddlewareConsumer, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CLientModule } from './client/client.module';
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
      //validate: validateENV,
    }),
    CLientModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        ...configService.get<DatabaseConfigInterface>('database'),
        entities: ListEntity,
        //  migrations: ['$database/migration/*.ts'],
        migrationsTableName: 'typeorm_migration',
        synchronize: true,
      }),
      inject: [ConfigService],
    }),
    ShareModule,
  ],

  providers: [
    { provide: APP_FILTER, useClass: AllExceptionsFilter },
    { provide: APP_INTERCEPTOR, useClass: LoggingInterceptor },
    { provide: APP_INTERCEPTOR, useClass: TransformDataInterceptor },
    { provide: APP_GUARD, useClass: JwtAuthGuard },
  ],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
