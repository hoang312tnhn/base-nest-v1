import { LiteralObject } from '@nestjs/common';

export function generateRandomCharacter(length: number) {
  let result = '';
  let characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export function returnPaging(
  data: LiteralObject,
  totalItems: number,
  params: LiteralObject,
  metadata = {},
) {
  return {
    pageIndex: params.pageIndex,
    totalPages: Math.ceil(totalItems / params.pageSize),
    totalItems,
    data,
    paging: true,
    metadata,
  };
}

export function returnLoadMore(
  data: LiteralObject,
  params: LiteralObject,
  metadata = {},
) {
  return {
    paging: true,
    hasMore: data.length === params.pageSize,
    data,
    pageSize: params.pageSize,
    ...metadata,
  };
}

export function assignLoadMore(params: LiteralObject) {
  params.pageSize = Number(params.pageSize) || 10;

  return params;
}

export function assignPaging(params: LiteralObject) {
  params.pageIndex = Number(params.pageIndex) || 1;
  params.pageSize = Number(params.pageSize) || 10;

  params.skip = (params.pageIndex - 1) * params.pageSize;
  params.take = params.pageSize;

  // delete params.pageSize;
  return params;
}
