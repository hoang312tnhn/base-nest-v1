import { UserType } from '$common/constant/enum';

export class AuthPayloadTokenDto {
  userId: string;
  userType: UserType;
}
