export class UserLoginDto {
  email: string;
  password: string;
}

export class UserRegisterDto {
  email: string;
  firstName?: string;
  lastName?: string;
}

export class UserUpdateProfileDto {
  firstName?: string;
  lastName?: string;
}

export class ChangePasswordDto {
  newPassword?: string;
  oldPassword?: string;
}
