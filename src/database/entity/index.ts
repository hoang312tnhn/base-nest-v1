import { Admin } from './Admin';
import { Photo } from './Photo';
import { User } from './User';

export const ListEntity = [User, Admin, Photo];
