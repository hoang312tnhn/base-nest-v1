import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './User';

@Entity()
export class Photo {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  title: string;

  @Column()
  src: string;

  @ManyToOne((type) => User)
  @JoinColumn({ referencedColumnName: 'id', name: 'user_id' })
  user: User;
}
