import { DataSource } from 'typeorm';
import { ListEntity } from './entity';
require('dotenv').config();
export const dataSource = new DataSource({
  type: 'mysql',
  host: process.env.MYSQL_HOST,
  port: 3306,
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DB,
  entities: ListEntity,
  migrations: ['./migration/*.ts'],
  migrationsTableName: 'typeorm_migration',
  subscribers: [],
  synchronize: true,
});
