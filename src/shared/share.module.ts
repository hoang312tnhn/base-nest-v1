import { Global } from '@nestjs/common';
import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { SendGridModule } from './send-mail/sendGrid.module';

@Global()
@Module({
  imports: [SendGridModule, AuthModule],
  exports: [SendGridModule, AuthModule],
})
export class ShareModule {}
