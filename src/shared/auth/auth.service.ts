import { AuthInterface } from '$config/interface/auth';
import { AuthPayloadTokenDto } from '$database/dto/PayloadToken.dto';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  private jwtConfig: AuthInterface;
  constructor(private jwtService: JwtService, private config: ConfigService) {
    this.jwtConfig = this.config.get<AuthInterface>('auth');
  }
  async createAccessToken(payload: AuthPayloadTokenDto) {
    return this.jwtService.sign(payload, {
      privateKey: this.jwtConfig.private_key_token,
      expiresIn: this.jwtConfig.token_expire_in,
    });
  }

  async verifyAccessToken(token) {
    const data = await this.jwtService.verifyAsync(token, {
      secret: this.jwtConfig.private_key_token,
    });
    return data;
  }

  async createRefreshToken(payload: AuthPayloadTokenDto) {
    const authEnv = this.config.get<AuthInterface>('auth');
    return this.jwtService.sign(payload, {
      privateKey: authEnv.private_key_refresh_token,
      expiresIn: authEnv.refresh_token_expire_in,
    });
  }

  async verifyRefreshToken(token) {
    const authEnv = this.config.get<AuthInterface>('auth');
    const data = await this.jwtService.verifyAsync(token, {
      secret: authEnv.private_key_refresh_token,
    });
    return data;
  }
}
