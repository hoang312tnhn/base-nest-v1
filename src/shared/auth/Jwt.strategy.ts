import { Unauthorized } from '$common/filters/exceptions/Unauthorized.exception';
import config from '$config/config';
import { AuthPayloadTokenDto } from '$database/dto/PayloadToken.dto';
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: config().auth.private_key_token,
    });
  }

  async validate(payload: AuthPayloadTokenDto) {
    return { userId: payload.userId, userType: payload.userType };
  }
}
