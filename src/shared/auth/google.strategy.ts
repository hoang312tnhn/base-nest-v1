import config from '$config/config';
import { AuthPayloadTokenDto } from '$database/dto/PayloadToken.dto';
import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-google-oauth20';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
  constructor() {
    super({
      clientID: config().auth_google.client_id,
      clientSecret: config().auth_google.client_secret,
      callbackURL: config().auth_google.callback_url,
      scope: ['email', 'profile'],
      failureRedirect: '/login/google',
    });
  }

  async validate(accessToken, refreshToken, profile, done) {
    const { name, emails, photos } = profile;
    console.log(profile);
    const user = {
      email: emails[0].value,
      firstName: name.givenName,
      lastName: name.familyName,
      picture: photos[0].value,
      accessToken,
    };
    //console.log(user);
    done(null, user);
  }
}
