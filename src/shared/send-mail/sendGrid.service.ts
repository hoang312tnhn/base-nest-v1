import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as SendGrid from '@sendgrid/mail';
@Injectable()
export class SendGridService {
  private readonly logger = new Logger(SendGridService.name);
  constructor(private readonly configService: ConfigService) {
    SendGrid.setApiKey(this.configService.get<string>('send_grid.api_key'));
  }

  async send(receiver: string, subject: string, content: string) {
    const msg = {
      to: receiver,
      from: this.configService.get<string>('send_grid.email'),
      subject,
      html: content,
    };
    SendGrid.send(msg)
      .then(() => {
        this.logger.log(`Send email to ${receiver} success!`);
      })
      .catch((err) => {
        console.log(err);
      });
  }
}
